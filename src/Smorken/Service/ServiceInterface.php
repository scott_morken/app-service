<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 7/24/14
 * Time: 7:49 AM
 */

namespace Smorken\Service;


interface ServiceInterface {

    /**
     * This is called when the service provider is created in App,
     * before it is loaded
     * @return null
     */
    public function start();

    /**
     * This is called when the service name is requested by App
     * @return null
     */
    public function load();

    /**
     * This returns the name of the service.  It is optional if
     * you handle the naming yourself.
     * @return string
     */
    public function getName();

    /**
     * This returns true or false whether the service is deferred or not.
     * Deferred means that the service objects will not be instantiated until
     * they are requested the first time.  Otherwise they will be instantiated
     * as soon as the service provider is created by App
     * @return bool
     */
    public function isDeferred();

    /**
     * This is an array of all the service names that this service provides provides.
     * It is needed to make sure that App loads all the service when needed.
     * @return array
     */
    public function provides();
} 