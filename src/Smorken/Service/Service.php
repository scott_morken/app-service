<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 7/24/14
 * Time: 7:50 AM
 */

namespace Smorken\Service;


class Service implements ServiceInterface {

    /**
     * @var \Smorken\Application\App
     */
    protected $app;
    /**
     * Load what the service provides immediately or wait
     * until it is called the first time?
     * @var bool
     */
    protected $deferred = false;
    /**
     * The name that is registered with App that this service will
     * provide
     * @var string
     */
    protected $name = 'service.abstract';

    /**
     * @param \Smorken\Application\App $app
     */
    public function __construct(\Smorken\Application\App $app)
    {
        $this->app = $app;
    }

    /**
     * Run before the service is loaded, set the name here or override and set it
     * above
     */
    public function start()
    {
        $this->name = 'service.abstract';
    }

    /**
     * get the service name
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Returns true or false if the service is deferred
     * @return bool
     */
    public function isDeferred()
    {
        return $this->deferred;
    }

    /**
     * Runs the service provider load
     * This is where you will want to bind/instance your service name onto the app
     * $this->app->bind('my_thing', function($app) { return new \stdClass(); });
     * @return \stdClass
     */
    public function load()
    {
        return new \stdClass();
    }

    /**
     * Provide an array of the service names that this service should return when it is loaded
     * eg if you want 'service1' and 'service2' to be loaded when the service is loaded, add
     * array('service1', 'service2') here
     * @return array
     */
    public function provides()
    {
        return array();
    }
} 