This project borrows heavily from the excellent [Laravel 4](http://laravel.com) and [Symfony 2](http://symfony.com) projects.

## License

This software is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

## Part of simple app

Should automatically be included if using composer to set up the project

Extend Smorken\Service\Service and add to config/app.php services

## Example

`src/Providers/TestService`

```
<?php
namespace App\Providers

class TestService extends Smorken\Service\Service
{
public function start()
    {
        $this->name = 'test.service';
    }

    public function load()
    {
        $app = $this->app;
        //use one of the following DI methods:
        //$this->app->instance will always return a single instance
        $this->app->instance($this->getName(), function($c) use ($app) {
            $test_config = $app['config']->get('test', array());
            return new \App\Test\TestHandler($test_config);
        });
        //$this->app['...'] will instantiate a new copy of the object on each call
        $this->app[$this->getName()] = function($c) {
            return new \App\Test\TestHandler();
        };
    }
}
```

`config/app.php`

```
<?php
return [
    ...,
    'services' => [
        ...,
        'App\Providers\TestService',
    ],
];
```